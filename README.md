# MPIODocker

Docker image with the [Alya MPI-IO tools](https://gitlab.com/bsc-alya/tools/alya-mpio-tools) compiled. 


The main application of this docker image is to run the postprocessing tools of Alya in a portable manner. 



