FROM ubuntu:20.04

#
# Install packages
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      software-properties-common \
 && apt-get update \
 && apt-get install -y \
      python-is-python3 \
 && apt-get install -y \
      python3 \
      python3-pip \
      python3-venv \
      curl \
      wget \
      tar \
      git \
      vim \
      build-essential \
 && rm -rf /var/lib/apt/lists/*

#
# Print python version
#
RUN set -x \
 && echo "PYTHON VERSION: " \
 && python --version


#
# Install OpenMPI as a dependency
# 
RUN set -x \
 && cd \
 && pwd \
 && wget https://download.open-mpi.org/release/open-mpi/v3.1/openmpi-3.1.6.tar.gz \
 && tar -xf openmpi-3.1.6.tar.gz \
 && rm -rf openmpi-3.1.6.tar.gz \
 && cd openmpi-3.1.6 \
 && mkdir build \
 && cd build \
 && ../configure --prefix=/usr/local/ \
 && make -j 4 all \
 && make install \
 && cd \
 && ls /usr/local/ \
 && rm -rf openmpi-3.1.6 \ 
 && ls 



#
# Install cmake dependencies
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      libssl-dev \
 && rm -rf /var/lib/apt/lists/* 

#
# Install cmake
#
RUN set -x \
 && cd \
 && pwd \
 && wget https://github.com/Kitware/CMake/releases/download/v3.20.0/cmake-3.20.0.tar.gz \
 && tar -xf cmake-3.20.0.tar.gz \
 && rm -rf cmake-3.20.0.tar.gz \
 && cd cmake-3.20.0 \
 && ./bootstrap \
 && make -j 4 \
 && make install \
 && cd \
 && cmake --version \
 && rm -rf cmake-3.20.0 \ 
 && ls 



#
# Install VTK as a dependency
#
RUN set -x \
 && cd \
 && pwd \
 && wget https://www.vtk.org/files/release/8.2/VTK-8.2.0.tar.gz \
 && tar -xf VTK-8.2.0.tar.gz \
 && rm -rf VTK-8.2.0.tar.gz \
 && cd VTK-8.2.0 \
 && mkdir build \
 && cd build \
 && cmake -DVTK_Group_Imaging=OFF -DVTK_Group_MPI=ON -DVTK_Group_Qt=OFF -DVTK_Group_Rendering=OFF -DVTK_Group_StandAlone=ON -DVTK_Group_Tk=OFF -DVTK_Group_Views=OFF -DVTK_Group_Web=OFF -DVTK_RENDERING_BACKEND=None .. \
 && make -j 4 \
 && make install \
 && cd \
 && ls /usr/local/lib/ \
 && rm -rf VTK-8.2.0 \
 && ls 



#
# Install ccmake and boost and package config
#
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      cmake-curses-gui \
      libboost-all-dev \
      pkg-config \
 && rm -rf /var/lib/apt/lists/* 



#
# Clone MPIO tools  
#
RUN set -x \
 && cd \
 && pwd \
 && git clone --recurse-submodules https://gitlab.com/bsc-alya/tools/alya-mpio-tools.git 2> /dev/null || (cd alya-mpio-tools; git pull ; cd ..) \
 && cd alya-mpio-tools \
 && ls 

##
## Check out specific version 
## (I am not using this, because there are no tags for the latest versions.)
##
#RUN set -x \
# && cd \
# && pwd \
# && cd alya-mpio-tools \
# && git fetch origin \
# && git checkout 7.3.0 \
# && git submodule update \
# && ls 


#
# MPI-IO tools build
#
RUN set -x \
 && cd \
 && pwd \
 && cd alya-mpio-tools \
 && rm -rf build \
 && mkdir -p build \
 && cd build \
 && cmake .. \
    -DVTK_DIR=/usr/local/lib/cmake/vtk-8.2 \
    -DCMAKE_C_COMPILER=/usr/local/bin/mpicc \
    -DCMAKE_CXX_COMPILER=/usr/local/bin/mpicxx \
    -DMPIEXEC_PREFLAGS='--allow-run-as-root;--oversubscribe' \
 && touch buildError.log \
 && make -j 4 install  2> buildError.log || (cat buildError.log) \
 && ls \
 && ls /usr/local/bin/ 


#
# More cleaning
#
RUN set -x \
 && cd \
 && pwd \
 && rm -rf alya-mpio-tools
